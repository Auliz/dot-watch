class CreateScreenplays < ActiveRecord::Migration[6.0]
  def change
    create_table :screenplays do |t|
      t.text :category
      t.text :picture_url
      t.text :title
      t.text :description
      t.integer :num_seasons
      t.integer :num_episodes
      t.integer :runtime

      t.timestamps
    end
  end
end
