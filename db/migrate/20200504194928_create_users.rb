class CreateUsers < ActiveRecord::Migration[6.0]
  def change
    create_table :users do |t|
      t.text :name
      t.text :username
      t.integer :password_digest
      t.text :bio
      t.integer :age
      t.text :location

      t.timestamps
    end
  end
end
